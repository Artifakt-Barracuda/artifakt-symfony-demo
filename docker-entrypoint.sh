#!/bin/bash

echo ">>>>>>>>>>>>>> START CUSTOM DOCKER ENTRYPOINT SCRIPT <<<<<<<<<<<<<<<<< "

printenv
pwd
printenv > dockerentrypoint

echo ">>>>>>>>>>>>>> END CUSTOM DOCKER ENTRYPOINT SCRIPT <<<<<<<<<<<<<<<<< "
